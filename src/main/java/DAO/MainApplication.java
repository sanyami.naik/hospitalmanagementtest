package DAO;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainApplication {
    static BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) throws IOException {

        DoctorDaoImplementation doctorDaoImplementation=new DoctorDaoImplementation();
        while(true)
        {
            System.out.println("1- For inserting");
            System.out.println("2- For retrieving");
            System.out.println("3- For updating");
            System.out.println("4- For deleting");
            System.out.println("5- For specialization");
            System.out.println("6- For rating greater than 5");
            System.out.println("7- For getting patient throuh doctors");
            System.out.println("Enter your choice");
            int option=Integer.parseInt(bufferedReader.readLine());
            switch(option)
            {
                case 1:
                    doctorDaoImplementation.insertDoctor();
                    break;

                case 2:
                    doctorDaoImplementation.retrieve();
                    break;

                case 3:
                    doctorDaoImplementation.updateDoctor();
                    break;

                case 4:
                    doctorDaoImplementation.deleteDoctor();
                    break;

                case 5:
                    doctorDaoImplementation.getSpecializedDoctor();
                    break;

                case 6:
                    doctorDaoImplementation.greaterRating();
                    break;

                case 7:
                    doctorDaoImplementation.patientThroughDoctor();
                    break;

            }
        }
    }
}

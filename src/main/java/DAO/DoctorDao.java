package DAO;

import java.io.IOException;

public interface DoctorDao {

    public void insertDoctor() throws IOException;
    public void updateDoctor() throws IOException;
    public void retrieve();
    public void deleteDoctor() throws IOException;
}

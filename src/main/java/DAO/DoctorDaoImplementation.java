package DAO;

import ENTITY.Doctor;
import ENTITY.Patient;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class DoctorDaoImplementation implements DoctorDao{

    static BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
    public void insertDoctor() throws IOException {

        EntityManagerFactory entityManagerFactory= Persistence.createEntityManagerFactory("hospital");
        EntityManager entityManager= entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        Doctor doctor=new Doctor();
        System.out.println("Enter the id of the doctor");
        int doctorId=Integer.parseInt(bufferedReader.readLine());
        System.out.println("Enter the doctor name");
        String doctorName= bufferedReader.readLine();
        System.out.println("Enter the experience of the doctor");
        int doctorexp=Integer.parseInt(bufferedReader.readLine());
        System.out.println("Enter the doctor specialization");
        String doctorSpecialization= bufferedReader.readLine();
        System.out.println("Enter the fees of the doctor");
        int doctorFees=Integer.parseInt(bufferedReader.readLine());
        doctor.setDoctorId(doctorId);
        doctor.setDoctorName(doctorName);
        doctor.setDoctorExperience(doctorexp);
        doctor.setDoctorFees(doctorFees);
        doctor.setDoctorSpecialization(doctorSpecialization);


        List<Doctor> doctorList=new ArrayList<>();
        doctorList.add(doctor);
        System.out.println("enter the the number of patients under this doctor");
        int patientNumber=Integer.parseInt(bufferedReader.readLine());

        List<Patient> patientList=new ArrayList<Patient>();
        Patient patient;
        for(int i=0;i<patientNumber;i++)
        {
            patient=new Patient();
            System.out.println("Enter the id of patient");
            int patientId=Integer.parseInt(bufferedReader.readLine());
            System.out.println("Enter the patient name");
            String patientName= bufferedReader.readLine();
            System.out.println("Enter the age of the patient");
            int patientAge=Integer.parseInt(bufferedReader.readLine());
            System.out.println("Enter the disease");
            String patientDisease= bufferedReader.readLine();
            System.out.println("Enter the feedback of the doctor");
            int feedback=Integer.parseInt(bufferedReader.readLine());

            patient.setPatientId(patientId);
            patient.setPatientName(patientName);
            patient.setPatientAge(patientAge);
            patient.setPatientDisease(patientDisease);
            patient.setFeedback(feedback);
            patient.setDoctorList(doctorList);

            patientList.add(patient);
            entityManager.persist(patient);

        }

        doctor.setPatientList(patientList);
        entityManager.persist(doctor);

        entityManager.getTransaction().commit();
entityManager.close();
entityManagerFactory.close();

    }

    public void updateDoctor() throws IOException {
        EntityManagerFactory entityManagerFactory1= Persistence.createEntityManagerFactory("hospital");
        EntityManager entityManager= entityManagerFactory1.createEntityManager();
        entityManager.getTransaction().begin();


        System.out.println("Enter the id of the doctor you want to update fees");
        int doctorId=Integer.parseInt(bufferedReader.readLine());
        Doctor doctor=entityManager.find(Doctor.class,doctorId);
        System.out.println("Enter the new fees");
        int newFees=Integer.parseInt(bufferedReader.readLine());
        doctor.setDoctorFees(newFees);

        System.out.println("Enter the id of the patient you want to update feedback of");
        int patientId=Integer.parseInt(bufferedReader.readLine());
        Patient patient=entityManager.find(Patient.class,patientId);
        System.out.println("Enter the new feedback");
        int newRating=Integer.parseInt(bufferedReader.readLine());
        patient.setFeedback(newRating);

        entityManager.getTransaction().commit();

    }

    public void retrieve() {
        EntityManagerFactory entityManagerFactory1= Persistence.createEntityManagerFactory("hospital");
        EntityManager entityManager1= entityManagerFactory1.createEntityManager();
        CriteriaBuilder criteriaBuilder1=entityManager1.getCriteriaBuilder();
        CriteriaQuery<Object> criteriaQuery1= criteriaBuilder1.createQuery();
        Root<Doctor> fromDoctor= criteriaQuery1.from(Doctor.class);
        Query query1=entityManager1.createQuery(criteriaQuery1.select(fromDoctor));
        List<Doctor> doctorList=query1.getResultList();
        System.out.println("The doctor List");
        doctorList.stream().forEach(System.out::println);

        EntityManagerFactory entityManagerFactory= Persistence.createEntityManagerFactory("hospital");
        EntityManager entityManager= entityManagerFactory.createEntityManager();
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaQuery<Object> criteriaQuery= criteriaBuilder.createQuery();
        Root<Patient> fromPatient= criteriaQuery.from(Patient.class);
        Query query=entityManager.createQuery(criteriaQuery.select(fromPatient));
        List<Patient> patientList=query.getResultList();


        System.out.println("********************************");
        System.out.println("The Patient List");
        patientList.stream().forEach(System.out::println);
}

    public void deleteDoctor() throws IOException {

        EntityManagerFactory entityManagerFactory1= Persistence.createEntityManagerFactory("hospital");
        EntityManager entityManager= entityManagerFactory1.createEntityManager();
        entityManager.getTransaction().begin();


        System.out.println("Enter the id of the doctor you want to delete");
        int doctorId=Integer.parseInt(bufferedReader.readLine());
        Doctor doctor=entityManager.find(Doctor.class,doctorId);
        entityManager.remove(doctor);
        entityManager.getTransaction().commit();
    }

    public void getSpecializedDoctor() throws IOException {
        EntityManagerFactory entityManagerFactory1= Persistence.createEntityManagerFactory("hospital");
        EntityManager entityManager1= entityManagerFactory1.createEntityManager();
        CriteriaBuilder criteriaBuilder1=entityManager1.getCriteriaBuilder();



        System.out.println("Enter the specialization of the doctor you want");
        String specialization=bufferedReader.readLine();
        Query query= entityManager1.createQuery("Select d from Doctor d where d.doctorSpecialization='"+specialization+"'");
        List<Object> li= query.getResultList();
        li.stream().forEach(System.out::println);

    }

    public void greaterRating() throws IOException {
        EntityManagerFactory entityManagerFactory= Persistence.createEntityManagerFactory("hospital");
        EntityManager entityManager= entityManagerFactory.createEntityManager();
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaQuery<Object> criteriaQuery= criteriaBuilder.createQuery();
        Root<Patient> fromPatient= criteriaQuery.from(Patient.class);


        System.out.println("The ratings greater than 5");
        Query query1=entityManager.createQuery(criteriaQuery.select(fromPatient).where(criteriaBuilder.gt(fromPatient.get("feedback"),5)));
        List<Patient> p=query1.getResultList();
        p.stream().forEach(System.out::println);

    }

    public void patientThroughDoctor() throws IOException {
        EntityManagerFactory entityManagerFactory= Persistence.createEntityManagerFactory("hospital");
        EntityManager entityManager= entityManagerFactory.createEntityManager();
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaQuery<Object> criteriaQuery= criteriaBuilder.createQuery();
        Root<Patient> fromPatient= criteriaQuery.from(Patient.class);

        System.out.println("Enter the doctor name");
        String doctorName= bufferedReader.readLine();
        Query query=entityManager.createQuery("Select d from Doctor d where d.doctorName= '"+doctorName+"'");
        List<Doctor> li=query.getResultList();
        li.stream().forEach(System.out::println);

    }
}
